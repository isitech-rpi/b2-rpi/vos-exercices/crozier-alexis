#pragma once

#include <iostream>
#include <string>

#include "Weapon.h"

namespace Weapons {
	class Fork : public Weapon {
	protected:
		Fork(float attack, float defense, int scope);

	public: 
		Fork();
	};

}