#include <iostream>
#include <string>
#include "POO.h"
#include "Hero.h"
#include "Weapon.h"
#include "Power.h"


using namespace std;

int main() {
	// Initialization a personnage 
	Personnages::Personnage guethenoc = Personnages::Personnage("Guethenoc");

	cout << "Information about the personnage : \n"
		<< "Name : "  << guethenoc.getName() << "\n"
		<< "Life : "  << guethenoc.getLife() << "\n"
		<< "Level : " << guethenoc.getLevel() << "\n\n"; 

	// Dynamic initialization
	Personnages::Personnage* balcmeg;
	balcmeg = new Personnages::Personnage("Balcmeg");

	cout << "Information about the dynamic personnage :  \n"
		<< "Name : "  << balcmeg->getName() << "\n"
		<< "Life : "  << balcmeg-> getLife() << "\n"
		<< "Level : " << balcmeg->getLevel() << "\n\n";

	// Initialization Hero 
	Personnages::Hero gollum = Personnages::Hero("Gollum");

	cout << "Information about Gollum :  \n"
		<< "Name : "  << gollum.getName() << "\n"
		<< "Life : "  << gollum.getLife() << "\n"
		<< "Level : " << gollum.getLevel() << "\n\n";

	// Dynamic initialization Hero
	Personnages::Hero* frodon;
	frodon = new Personnages::Hero("Frodon", 10);

	cout << "Information about the dynamic Hero :  \n"
		<< "Name : "  << frodon->getName() << "\n"
		<< "Life : "  << frodon->getLife() << "\n"
		<< "Level : " << frodon->getLevel() << "\n\n";

	// Attack game
	guethenoc.attack(balcmeg);

	cout << "Stat guethenoc after attack (attacker) : \n"
		<< "Name : " << guethenoc.getName() << "\n"
		<< "Life : " << guethenoc.getLife() << "\n"
		<< "Stat balcmeg after attack (defenser) : \n"
		<< "Name : " << balcmeg->getName() << "\n"
		<< "Life : " << balcmeg->getLife() << "\n\n";

	frodon->attack(&gollum);

	cout << "Stat Frodon after attack (attacker) : \n"
		<< "Name : " << frodon->getName() << "\n"
		<< "Life : " << frodon->getLife() << "\n"
		<< "Stat Gollum after attack (defenser) : \n"
		<< "Name : " << gollum.getName() << "\n"
		<< "Life : " << gollum.getLife() << "\n\n";

	// Weapon
	Weapons::Weapon weapon1 = Weapons::Weapon::Weapon();
	Weapons::Weapon* weapon2;
	weapon2 = new Weapons::Weapon(2, 3, 4);

	guethenoc.collectWeapon(&weapon1);
	gollum.collectWeapon(&weapon1);

	frodon->collectWeapon(weapon2);

	guethenoc.attack(balcmeg);
	cout << "Stat guethenoc after attack (attacker) : \n"
		<< "Name : " << guethenoc.getName() << "\n"
		<< "Life : " << guethenoc.getLife() << "\n"
		<< "Stat balcmeg after attack (defenser) : \n"
		<< "Name : " << balcmeg->getName() << "\n"
		<< "Life : " << balcmeg->getLife() << "\n\n";

	frodon->attack(&gollum);
	cout << "Stat Frodon after attack (attacker) : \n"
		<< "Name : " << frodon->getName() << "\n"
		<< "Life : " << frodon->getLife() << "\n"
		<< "Stat Gollum after attack (defenser) : \n"
		<< "Name : " << gollum.getName() << "\n"
		<< "Life : " << gollum.getLife() << "\n\n";


	// Composition

	Powers::Power* spider_web;
	spider_web = new Powers::Power("spider's web");
	Personnages::Hero* spiderman;
	spiderman = new Personnages::Hero("Peter Parker", &spider_web);

	cout << &spiderman->getPower();

	return 0;
}