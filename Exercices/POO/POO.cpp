// POO.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>
#include <string>
#include "POO.h"
#include "Weapon.h"

using namespace std;


Personnages::Personnage::Personnage(string name, float life, int level)
{
	this->name  = name;
	this->life  = life;
	this->level = level;
}
;

bool Personnages::Personnage::defense(Personnage* attacker)
{
	if (this->isAlive()) {
		float lifeAttacker = attacker->getLife() - (this->getLife() * this->getLevel() / 20);

		if (lifeAttacker < 0) {
			lifeAttacker = 0;
		}

		attacker->setLife(lifeAttacker);
	}

	return this->isAlive();
}


Personnages::Personnage::Personnage()
{
}

Personnages::Personnage::Personnage(string name)
{
	this->name  = name;
}
;

string Personnages::Personnage::getName()
{
	return this->name;
}

float Personnages::Personnage::getLife()
{
	return this->life;
}

int Personnages::Personnage::getLevel()
{
	return this->level;
}

float Personnages::Personnage::setLife(float life)
{
	this->life = life;

	return this->life;
}

bool Personnages::Personnage::attack(Personnage* ennemi)
{
	if (ennemi->isAlive()) {
		float lifeEnnemi;

		if (!(this->currentWeapon == nullptr)) {
			if (this->getWeapon()->use(true)) {
				lifeEnnemi = ennemi->getLife() - (this->getLife() + this->currentWeapon->getAttack() * this->getLevel() / 20);
			}
			else {
				lifeEnnemi = ennemi->getLife() - (this->getLife() + this->currentWeapon->getDefense() * this->getLevel() / 20);
			}
		}
		else {
			lifeEnnemi = ennemi->getLife() - (this->getLife() * this->getLevel() / 20);
		}

		if (lifeEnnemi < 0) {
			lifeEnnemi = 0;
		}

		ennemi->setLife(lifeEnnemi);

		ennemi->defense(this);
	}

	return ennemi->isAlive();
}

bool Personnages::Personnage::isAlive()
{
	return this->getLife() > 0;
}

Weapons::Weapon* Personnages::Personnage::collectWeapon(Weapons::Weapon* weapon)
{
	this->currentWeapon = weapon;
	
	return this->currentWeapon;
}

Weapons::Weapon* Personnages::Personnage::getWeapon()
{
	return this->currentWeapon;
}
