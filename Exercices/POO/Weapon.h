#pragma once

#include <iostream>
#include <string>

namespace Weapons {
	class Weapon {
	protected:
		float attackPower  = 2;
		float defensePower = 2;
		int   scope        = 1;


	public:
		Weapon();

		Weapon(float attack, float defense, int scope);

		float use(bool attack);

		float getAttack() { return this->attackPower; };
		float getDefense() { return this->defensePower; };
		int   getScope() { return this->scope; };
	};
}