#pragma once

#include "POO.h"
// #include "Power.h"
#include <iostream>
#include <string>

namespace Powers {
	class Power;
}

namespace Personnages {
	class Hero : public Personnage {
	    protected:
			Powers::Power* power;

		public:
			Hero();

			Hero(string name);

			Hero(string name, int level);

			Hero(string name, Powers::Power power);

			Powers::Power getPower();

			//virtual ~Hero();
	};
}
