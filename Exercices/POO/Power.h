#pragma once

#include <iostream>
#include <string>
#include "Hero.h"

using namespace std;

namespace Powers {
	class Power {
	protected:
		string name;

		Personnages::Hero* hero;

	public:
		Power(string name);

		Powers::Power invoke();
	};
}