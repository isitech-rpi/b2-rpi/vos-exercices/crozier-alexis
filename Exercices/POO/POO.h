#pragma once

#include <iostream>
#include <string>
#include "Weapon.h"

using namespace std;

namespace Personnages {
	class Personnage {
	protected:
		float life = 10.0;
		int   level = 1;

		Weapons::Weapon* currentWeapon = nullptr;

		// Methods
		bool defense(Personnage* attacker);

	private:
		string name;

	public:
		// Association

		// Constructor
		Personnage();
		Personnage(string name);
		Personnage(string name, float life, int level);

		// getter
		string getName();
		float  getLife();
		int    getLevel();

		// setter
		float setLife(float life);

		// Methods
		bool attack(Personnages::Personnage* ennemi);
		bool isAlive();

		Weapons::Weapon* collectWeapon(Weapons::Weapon* weapon);
		Weapons::Weapon* getWeapon();

		// destructor
		//virtual ~Personnage();
	};
}


