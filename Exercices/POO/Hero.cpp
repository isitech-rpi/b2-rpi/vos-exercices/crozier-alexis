#include <iostream>
#include <string>
#include "Hero.h"
#include "POO.h"
#include "Power.h"

Personnages::Hero::Hero()
{
}

Personnages::Hero::Hero(string name) : Personnage(name) {
}

Personnages::Hero::Hero(string name, int level = 2) : Personnage(name) {
	this->level = level;
}

Personnages::Hero::Hero(string name, Powers::Power power) : Personnage(name)
{
	this->power = &power;
}

Powers::Power Personnages::Hero::getPower()
{
	return this->power;
}
