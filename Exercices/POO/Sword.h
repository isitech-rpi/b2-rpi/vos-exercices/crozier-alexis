#pragma once

#include <iostream>
#include <string>

#include "Weapon.h"

namespace Weapons {
	class Sword : public Weapon {
	protected:
		Sword(float attack, float defense, int scope);

	public:
		Sword();

		~Sword();

		float use(bool attack);
	};
}