#include "Sword.h"

Weapons::Sword::Sword()
{
}

Weapons::Sword::~Sword()
{
}

float Weapons::Sword::use(bool attack)
{
	return Weapons::Weapon::use(attack);
}

Weapons::Sword::Sword(float attack, float defense, int scope)
{
	this->attackPower = attack;
	this->defensePower = defense;
	this->scope = scope;
}

