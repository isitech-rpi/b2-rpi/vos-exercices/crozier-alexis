#include "Power.h"
#include <iostream>
#include <string>

using namespace std;

Powers::Power::Power(string name)
{
	this->name = name;
}

Powers::Power Powers::Power::invoke()
{
	cout << "Powers " << this->name << " is invoked by " << this->hero->getName() << "\n\n";
}
