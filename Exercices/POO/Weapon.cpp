#include <iostream>
#include <string>
#include "Weapon.h"

using namespace std;

Weapons::Weapon::Weapon()
{
}

Weapons::Weapon::Weapon(float attack, float defense, int scope)
{
	this->attackPower  = attack;
	this->defensePower = defense;
	this->scope        = scope;
}

float Weapons::Weapon::use(bool attack)
{
	if (attack) {
		cout << "Stat attack weapon : "
			<< this->attackPower
			<< "\n\n";

		return this->attackPower;
	}
	else {
		cout << "Stat defense weapon : "
			<< this->defensePower
			<< "\n\n";

		return this->defensePower;
	}
}



