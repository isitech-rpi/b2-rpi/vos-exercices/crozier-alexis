#include <iostream>
#include <iomanip>
#include "Fonctions.cpp"
#undef max

using namespace std;

int main() {
	// Exercise 1.1
	// Variables 
	int   a = 1;
	int   b = 5;
	float f = 3.14;
	char  c = 'C';
	char  d;

	cout << "Display all variables and their address \n"
		 << a << ' ' << hex << &a << "\n"
		 << b << ' ' << hex << &b << "\n"
		 << f << ' ' << hex << &f << "\n"
		 << c << ' ' << hex << &c << "\n"
		 << d << ' ' << hex << &d << "\n";


	d = 'D';

	cout << "Variable 'd' is equal to " << d << " and his address is " << hex << &d << "\n";

	a = 5;
	b = 1;

	cout << "New a value " << a << " and her address " << &a << "\n"
		<< "New b value " << b << " and her address " << &b << "\n";

	a = f;
	f = 1; 

	cout << "New a value " << a << " and her address " << &a << "\n"
		 << "New f value " << f << " and her address " << &f << "\n";

	d = 90;

	cout << "Variable 'd' is equal to " << d << dec << " and his numeric value is " << static_cast<int>(d) << "\n";

	d = d + 255;

	cout << "Variable 'd' is equal to " << d << dec << " and his numeric value is " << static_cast<int>(d) << "\n";


	// Exercise 1.2
	int i1 = 1, i2 = 3, r1;

	r1 = i1 / i2;

	cout << "r1 = " << r1 << "\n";

	float d1 = 1.0, d2 = 3.0, r2;
	r2 = d1 / i2;
	cout << "r2 = d1 / i2 = " << r2 << "\n";

	r2 = i1 / i2;
	cout << "r2 = d1 / i2 = " << r2 << "\n";

	r2 = i1 / d2;
	cout << "r2 = i1 / d2 = " << r2 << "\n";

	r2 = d1 / d2;
	cout << "r2 = d1 / d2 = " << r2 << "\n\n";

	// Exercice 1.3
	int newA = 3, newB = 10;
	
	int* p;
	p = &newA;

	p = &newB;
	cout << "Pointeur p (valeur point�e) = " << *p << " ayant pour addresse : " << &p << " et pointant l'addresse " << p << "\n";

	p = &newA;
	int newAFactor2 = *p * 2;
	cout << "La valeur point�e par p (newA) multipli�e par 2 est " << newAFactor2 << "\n";

	*p = *p + 1;
	cout << "*p +1 = " << *p << "\n\n";

	// Exercice 2
	// Exercice 2.1
	int hours, minutes;

	cout << "Please enter the hour : \n";
	while (!(cin >> hours) || (hours > 23)) {
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		cout << endl << "Wrong type of data, please enter a number \n";
	}

	cout << "Please now enter minutes : \n";
	while (!(cin >> minutes) || (minutes > 59)) {
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		cout << endl << "Wrong type of data, please enter a number \n";
	}

	if ((minutes == 59) && (hours < 23)) {
		hours = hours + 1;
		minutes = 0;
	}
	else if ((hours == 23) && (minutes == 59)) {
		hours   = 0;
		minutes = 0;
	}
	else {
		minutes = minutes + 1;
	}

	cout << "I can tell you that the nex minute will be " << setw(2) << setfill('0') << hours << ":" << setw(2) << setfill('0') << minutes << "\n\n";

	// Exercice 2.2
	int age;

	cout << "Please enter the age of the challenger : \n";
	while (!(cin >> age)) {
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		cout << endl << "Wrong type of data, please enter a number \n";
	}

	if (age < 6) {
		cout << "Hors cat�gorie \n";
	}
	else {
		switch (age) {
		case 6:
		case 7:
		case 8:
		case 9:
			cout << " Cat�gorie pr�-poussin \n" << endl;
			break;

		case 10:
		case 11:
			cout << "Cat�gorie poussin \n" << endl;
			break;

		case 12:
		case 13:
			cout << "Cat�gorie benjamin \n" << endl;
			break;
		case 14:
		case 15:
			cout << "Cat�gorie minime \n" << endl;
			break;

		case 16:
		case 17:
			cout << "Cat�gorie cadet \n" << endl;

		default:
			cout << "Cat�gorie s�nior \n" << endl;
			break;
		}
	};

	// Exercice 2.3
	int iterator = 0;
	int sum = 0;
	int mark;
	int average;

	do {
		cout << "Please enter the mark (to stop the program enter -1) : \n";
		cin  >> mark;
		iterator = iterator + 1;
		sum = sum + mark;
	}
	while (mark != (-1));

	average = sum / iterator;
	cout << "The average = " << average << "\n\n";

	// Exercice 3
	// 3.1
	inputIntBetween(2, 10);


	//3.2
	int gameChosen;

	cout << "Please choose a game between 32 or 54 : ";

	cin >> gameChosen;

	pickCard(gameChosen);


	// Tableaux
	// 1.1
	const int collectionSize = 10;
	int collection[collectionSize] = {};

	remplirTab(collection, collectionSize);

	cout << "Tableau [";
	for (int item : collection) {
		cout << item << " ";
	};
	cout << "] \n\n";

	//1.2
	copie(collection, collectionSize);

	//1.3
	sort(collection, collectionSize, true);
}
