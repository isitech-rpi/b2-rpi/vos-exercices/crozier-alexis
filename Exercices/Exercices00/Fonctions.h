int inputIntBetween(int min, int max);

int pickCard(int game);

void remplirTab(int collection[], int collectionSize);

void copie(int collection[], int collectionSize);

void sort(int collection[], int collectionSize, bool asc);