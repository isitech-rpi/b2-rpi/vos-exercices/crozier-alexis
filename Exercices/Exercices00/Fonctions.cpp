#include "Fonctions.h"
#include <iostream>
#include <random>
#include <string>
#include <algorithm>

using namespace std;

int inputIntBetween(int min, int max) {
	int value;

	cout << "Enter a number : \n";
	cin  >> value;

	if ((value > min) && (value < max)) {
		cout << "Value conform" << "\n\n";
	}
	else {
		cout << "Value incorrect" << "\n\n";
	}
	
	return 0;
}

int pickCard(int game) {
	const char *game32[] = {
		"as_coeur", "roi_coeur", "dame_coeur", "valet_coeur", "10_coeur", "9_coeur", "8_coeur", "7_coeur",
		"as_trefle", "roi_trefle", "dame_trefle", "valet_trefle", "10_trefle", "9_trefle", "8_trefle", "7_trefle",
		"as_pique", "roi_pique", "dame_pique", "valet_pique", "10_pique", "9_pique", "8_pique", "7_pique",
		"as_carreau", "roi_carreau", "dame_carreau", "valet_carreau", "10_carreau", "9_carreau", "8_carreau", "7_carreau"
	};

	const char *game54[] = {
		"as_coeur", "2_coeur", "3_coeur", "4_coeur", "5_coeur", "6_coeur", "7_coeur", "8_coeur", "9_coeur", "10_coeur", "valet_coeur", "dame_coeur", "roi_coeur",
		"as_trefle", "2_trefle", "3_trefle", "4_trefle", "5_trefle", "6_trefle", "7_trefle", "8_trefle", "9_trefle", "10_trefle", "valet_trefle", "dame_trefle", "roi_trefle",
		"as_pique", "2_pique", "3_pique", "4_pique", "5_pique", "6_pique", "7_pique", "8_pique", "9_pique", "10_pique", "valet_pique", "dame_pique", "roi_pique",
		"as_carreau", "2_carreau", "3_carreau", "4_carreau", "5_carreau", "6_carreau", "7_carreau", "8_carreau", "9_carreau", "10_carreau", "valet_carreau", "dame_carreau", "roi_carreau",
		"joker_noir", "joker_rouge"
	};

	cout << "jeu : " << game << "\n\n";

	int randomCard;

	if (game == 32) {
		randomCard = rand() % (sizeof(game32) / sizeof(game32[0]));

		cout << "random  " << randomCard << "\n\n" << "Card 32 picked is : " << game32[randomCard] << "\n\n";
	}
	else if (game == 54) {
		randomCard = rand() % (sizeof(game54) / sizeof(game54[0]));

		cout << "Card 54 picked is : " << game54[randomCard] << "\n\n";
	}

	return 0;
}

void remplirTab(int collection[], int collectionSize) {
	int item;

	for (int i = 0; i < collectionSize; i++) {
		cout << "Entrer une valeur a enregistrer : \n";
		cin >> item;

		collection[i] = item;
	}

	cout << "Tableau complet \n\n";
}

void copie(int collection[], int collectionSize) {
	int* ptr_collection;
	int* ptr_new_collection;

	ptr_collection = collection;
	ptr_new_collection = new int[collectionSize];

	cout << "Copie tableau : [";
	for (int i = 0; i < collectionSize; i++) {
		ptr_new_collection[i] = collection[i];
		cout << ptr_new_collection[i] << " ";
	};
	cout << "] \n\n";

	delete ptr_new_collection;
}

void sort(int collection[], int collectionSize, bool asc) {
	if (sort) {
		sort(collection, collection + collectionSize);
	}
	else {
		sort(collection, collection + collectionSize, greater<int>());
	}

	cout << "Tableau : [";
	for (int i = 0; i < collectionSize; i++) {
		cout << collection[i] << " ";
	};
	cout << "] \n\n";
}