#include <iostream>
#include <string>
#include "Classes.h"

using namespace std;

int main() {
	// Static
	Cours matiere_1 = Cours("c++", "b2-RPI");

	cout << matiere_1.getMatiere() << " " << matiere_1.getPromo() << "\n\n";

	// Dynamic
	Cours* matiere_2;
	matiere_2 = new Cours("php", "3olen");

	cout << matiere_2->getMatiere() << " " << matiere_2->getPromo() << "\n\n";

	// Array static 
	Cours uwp = Cours("UWP", "B3-WTech");
	Cours uml = Cours("UML", "b2-DevWeb");
	Cours Cours_1[2] = {uwp, uml};

	int arraySize = sizeof(Cours_1) / sizeof(Cours_1[0]);
	
	for (int i = 0; i < arraySize; i++) {
		cout << Cours_1[i].getMatiere() << " " << Cours_1[i].getPromo() << "\n";
	}

	// Array dynamic
	Cours git = Cours("GIT", "3olen");
	Cours php = Cours("PHP", "B2-WTech");
	Cours* Cours_2 = new Cours[2];
	Cours_2[0] = git;
	Cours_2[1] = php;

	for (int i = 0; i < 2; i++) {
		cout << Cours_2[i].getMatiere() << " " << Cours_2[i].getPromo() << "\n";
	}

	// Modification of promos
	Cours_1[0].setPromo("B2 - DEV");
	Cours_2[0].setPromo("B2 - DEV");
	Cours_2[1].setPromo("B2 - DEV");

	// Deletion
	delete[] Cours_2;
	delete matiere_2;

	return 0;
}