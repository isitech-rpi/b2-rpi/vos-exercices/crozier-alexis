#include <string>
#include <iostream>
#include "Classes.h"

using namespace std;

Cours::Cours() : Cours("dessin", "CM2")
{
}

Cours::Cours(string matiere, string promo) {
	this->matiere = matiere;
	this->promo = promo;
}

Cours::~Cours() {
	this->setPromo("");
	this->setMatiere("");
}

string Cours::getPromo() {
	return this->promo;
}

string Cours::getMatiere() {
	return this->matiere;
}

string Cours::setPromo(string promo) {
	this->promo = promo;
	return this->promo;
}

string Cours::setMatiere(string matiere) {
	this->matiere = matiere;
	return this->matiere;
}

void Cours::Enseigner(string matiere, string promo) {
	cout << "enseigne " << matiere << " a la promotion " << promo << "\n\n";
};