#pragma once

#include <string>

using namespace std;

class Cours
{
private:
	string matiere;
	string promo;

public:
	Cours();

	Cours(string matiere, string promo);

	virtual ~Cours();

	// getter
	string getPromo();
	string getMatiere();

	// setter
	string setPromo(string promo);
	string setMatiere(string matiere);

	// method
	void Enseigner(string matiere, string promo);
};