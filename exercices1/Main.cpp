#include <iostream>

using namespace std;

int main() {
	// Variables 
	int   a = 1;
	int   b = 5;
	float f = 3.14;
	char  c = 'C';
	char  d;

	cout << "Display all variables and their address \n"
		 << a << ' ' << hex << &a << "\n"
		 << b << ' ' << hex << &b << "\n"
		 << f << ' ' << hex << &f << "\n"
		 << c << ' ' << hex << &c << "\n"
		 << d << ' ' << hex << &d << "\n";


	d = 'D';

	cout << "Variable 'd' is equal to " << d << " and his address is " << hex << &d << "\n";

	a = 5;
	b = 1;

	cout << "New a value " << a << " and her address " << &a << "\n"
		<< "New b value " << b << " and her address " << &b << "\n";

	a = f;
	f = 1; 

	cout << "New a value " << a << " and her address " << &a << "\n"
		 << "New f value " << f << " and her address " << &f << "\n";

	d = 90;

	cout << "Variable 'd' is equal to " << d << " and his numeric value is " << static_cast<int>(d) << "\n";

	d = d + 255;

	cout << "Variable 'd' is equal to " << d << " and his numeric value is " << static_cast<int>(d) << "\n";


}
